const express = require("express");
const expressLayouts = require("express-ejs-layouts");
const {
  loadData
} = require("./utils/dataCars");
const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.urlencoded());
app.set("view engine", "ejs");
app.use(expressLayouts);
app.use(express.static(__dirname + "/public"));

app.get("/", (req, res) => {
  const data = loadData();
  res.render("pages/carList", {
    layout: "layouts/main-layout",
    title: "Car List",
    data,
  });
});

app.get("/AddCars", (req, res) => {
  res.render("pages/addCars", {
    layout: "layouts/main-layout",
    title: "Add New Car",
  });
});

app.get("/editCars", (req, res) => {
  res.render("pages/editCars", {
    layout: "layouts/main-layout",
    title: "Update Car Information",
  });
});

app.use((req, res) => {
  res.status(404).render("pages/error404", {
    layout: "layouts/main-layout",
    title: "404",
  });
});

app.listen(PORT, () => {
  console.log(`Server is now running on http://localhost:${PORT}`);
});
